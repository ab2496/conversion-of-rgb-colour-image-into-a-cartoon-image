# Conversion of rgb colour image into a cartoon image

In these,the image is cartoonized using a bilateral filter. Bilateral filtering is an edge-preserving smoothing filter that can be used for a wide variety of image processing tasks such as de-noising and tone mapping, and cartoonizing the image.
